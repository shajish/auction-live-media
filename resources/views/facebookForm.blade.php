@extends('layouts.app')
@section('content')

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>
    //variable declarations
    var appID, appVersion, appScopes;

    //variable values
    appID = '227777042284833';
    appVersion = 'v9.0'
    appScopes = 'public_profile,email,publish_video,pages_show_list,publish_to_groups,pages_read_engagement,pages_manage_metadata,pages_manage_posts';

    window.fbAsyncInit = function() {
        FB.init({
            appId: appID,
            cookie: true,
            xfbml: true,
            version: appVersion
        });
        FB.getLoginStatus(function(response) { // Called after the JS SDK has been initialized.
            statusChangeCallback(response); // Returns the login status.
        });

        // publish content
        // FB.ui({
        //     method: 'share',
        //     href: 'https://i.ytimg.com/vi/FAciZRkOKQs/maxresdefault.jpg'
        // }, function(response) {});
    };


    function statusChangeCallback(response) { // Called with the results from FB.getLoginStatus().
        if (response.status === 'connected') { // Logged into your webpage and Facebook.
            console.log('you are connected ');
        } else { // Not logged into your webpage or we are unable to tell.
            FB.login(function(response) {
                console.log(response.authResponse);
                if (response.authResponse) {
                    document.getElementById('user_id').value = response.authResponse.userID;
                    document.getElementById('token').value = response.authResponse.accessToken;
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', function(response) {
                        console.log('Good to see you, ' + response.name + '.');
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, {
                scope: appScopes
            });
        }
    }
</script>
<div class="form-container box-shadow">

    <div class="content">
        <div class="info">
            <h2>INFORMATION</h2>
            <p> Please generate <strong>Access token</strong> from your facebook developer app-dashboard.</p>
        </div>
        <div class="form">
            <h2>FACEBOOK FORM</h2>
            <form method="POST" action="/facebook" enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label>Message</label>
                    <input type="text" class="form-control @error('message') is-invalid @enderror" name="message" id="text">
                </div>

                <div class="form-group ">
                    <label>image</label>
                    <input type="file" class="form-control @error('file') is-invalid @enderror" name="image" multiple>
                </div>
                <!--
                    TODO TO BE ADDED HERE FOR BETTER DESIGN
                    @error('image')
                    <div class="form-group">
                        <div class="alert alert-danger">{{ $message }}</div>
                    </div>
                    @enderror
                -->
                <div class="form-group ">
                    <label>Image url</label>
                    <input type="text" class="form-control @error('link') is-invalid @enderror" name="link" id="link">
                </div>

                <div class="form-group">
                    <input type='hidden' name='user_id' value='' id='user_id'>
                    <input type='hidden' name='user_access_token' value='' id='token'>
                    <input type="submit" class="form-control button" name="submit" value="submit">
                </div>
            </form>
        </div>
    </div>

    <div class="error-container"> @if(Session::has('success'))
        <div class="alert alert-success">
            {{
                Session::get('success')
            }}
        </div>
        @endif
        @if (session('test'))
        <div class="alert alert-danger" role="alert">
            {{session('test')}}
        </div>
        @endif
        @error('user_access_token')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('message')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('image')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @error('link')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>


</div>




@endsection