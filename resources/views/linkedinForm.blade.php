@extends('layouts.app')
@section('content')
<div class="container">
    <div class="form-container box-shadow" style="height: auto;">
        <div class="content">
            <div class="info">
                <h2>INFORMATION</h2>
                <p>
                    please use the link below and once you login, grab the authorization code from the URL
                </p>
                <p><u>https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86pi28nbc2mypq&redirect_uri=https://www.google.com&state=DCEeFWf45A53sdfKef424&scope=r_liteprofile r_emailaddress w_member_social</u> </p>
                <p>
                    use the authentication code and hit this link using postman to get access token.</p>
                <p>
                    <u>https://www.linkedin.com/oauth/v2/accessToken</u>
                <p></p>
                params: grant_type, code, redirect_uri,client_id,client_secret
                </p>
                <p>
                    now use the access_token to enjoy this service.
                </p>
            </div>
            <div class="form">
                <h2>LINKEDIN FORM</h2>
                <form method="POST" action="/linkedin" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label>Access Token</label>
                        <input type="text" class="form-control" name="access_token" id="access_token">
                    </div>

                    <div class="form-group row">
                        <label>Text</label>
                        <input type="text" class="form-control" name="text" id="text">
                    </div>

                    <div class="form-group row">
                        <label>Image</label>
                        <input type="file" class="form-control" name="image">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="form-control button" name="submit" value="Submit">
                    </div>

                </form>
            </div>
        </div>
        <div class="error-container">
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{
                Session::get('success')
                }}
            </div>
            @endif
            @error('access_token')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('text')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            @error('image')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

        </div>
    </div>

</div>
@endsection