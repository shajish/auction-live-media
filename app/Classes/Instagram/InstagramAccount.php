<?php

namespace App\Classes\Instagram;

use App\Classes\Facebook\FacebookAccount;
use Exception;

class InstagramAccount extends FacebookAccount
{
    protected $instagram_business_account_id;

    /**
     * __construct
     *
     * @param  mixed $user_access_token
     * @param  mixed $page_id
     * @return void
     */
    public function __construct($user_access_token, $page_id)
    {
        parent::__construct();
        $this->getInstagramBusinessAccounts($user_access_token, $page_id);
        $this->long_lived_user_access_token= $user_access_token;
    }

    /**
     * getInstagramBusinessAccounts
     * 
     * Gets the business instagram page of the associated to the Facebook page
     *
     * @param  mixed $user_access_token
     * @param  mixed $user_id
     * @param  mixed $page_id
     * @return void
     */
    public function getInstagramBusinessAccounts($user_access_token, $page_id)
    {
        try {
            $response = $this->fb->sendRequest(
                'GET',
                '/' . $page_id,
                [
                    "fields" => "instagram_business_account",
                    "access_token" => $user_access_token
                ],
            );
            $data = json_decode($response->getBody(), true);
            $this->instagram_business_account_id = $data['instagram_business_account']['id'];
            return response()->json($data, 200);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
