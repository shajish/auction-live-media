<?php
namespace App\Classes\Instagram;

use App\Classes\Facebook\FacebookAccount;
use Exception;

class InstagramPost extends InstagramAccount{

    public function getAllPosts(){
        try {
            $response = $this->fb->sendRequest(
                'GET',
                '/' . $this->instagram_business_account_id.
                '/media',
                [
                    "fields" => "instagram_business_account",
                    "access_token" => $this->long_lived_user_access_token
                ],
            );
            $data = json_decode($response->getBody(), true);
            dd($data);
            return response()->json($data, 200);
        } catch (Exception $e) {
            throw $e;
        }
           
    }
}