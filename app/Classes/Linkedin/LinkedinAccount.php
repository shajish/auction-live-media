<?php

namespace App\Classes\Linkedin;

use Illuminate\Support\Facades\Http;

class LinkedinAccount
{

    private $authorizationTokenURL = "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=86pi28nbc2mypq&redirect_uri=https://www.google.com&state=DCEeFWf45A53sdfKef424&scope=r_liteprofile r_emailaddress w_member_social";
    private $accesstokenURL = "https://www.linkedin.com/oauth/v2/accessToken";
    protected $URI = 'https://api.linkedin.com/v2';
    public function __construct()
    {
        //get the access token $authorization token -> $access_token
    }

    /**
     * Fetches user details
     * response data:
     * person URN, first name, last name, profilepicure (text)
     */
    public function getUserProfile($access_token)
    {
        $response = Http::withToken($access_token)->get($this->URI . "/me")->throw()->json();
        return $response;
    }
}
