<?php

namespace App\Classes\Linkedin;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class LinkedinPost extends LinkedinAccount
{

    private $person_id;
    private $access_token;
    private $imageAsset;
    private $imageUploadURL;

    /**
     * permission required = w_member_social
     * must have header: X-Restli-Protocol-Version: 2.0.0
     */
    public function publishContents(Request $request)
    {
        $userProfile = $this->getUserProfile($request->access_token);
        $this->person_id = $userProfile['id'];
        $this->access_token = $request->access_token;

        switch ($request->type) {
            case "onlyText":
                $response = $this->onlyText($request->input('text'));
                return $response;
                break;
            case "withImage":
                //register image upload process
                $this->registerMediaUpload();
                //upload the image
                $uploadResponse = $this->uploadImage($request->file('image'));
                //check upload status of the file
                if ($uploadResponse->getStatusCode() == 201) {
                    $this->checkUploadStatus();
                    //Post publicly
                    $postResponse = $this->textAndImage($request->input('text'));
                    return $postResponse;
                } else {
                    //throwing exception if anything wrong
                    throw new Exception("upload image failed", 500);
                }
                break;
        }
    }
  
    /**
     * onlyText
     *
     * @param  mixed $text
     * @return void
     */
    public function onlyText(string $text)
    {
        $response = Http::withToken($this->access_token)->post(
            $this->URI . '/ugcPosts',
            [
                "author" => "urn:li:person:" . $this->person_id,
                "lifecycleState" => "PUBLISHED",
                "specificContent" => [
                    "com.linkedin.ugc.ShareContent" => [
                        "shareCommentary" => ["text" => $text],
                        "shareMediaCategory" => "NONE",
                    ],
                ],
                "visibility" => ["com.linkedin.ugc.MemberNetworkVisibility" => "PUBLIC"]
            ]
        )->throw()->json();
        return response()->json($response, 200);
    }

    /**
     * textAndImage
     * Post on linkedin user profile with text and single image
     * @param  mixed $text
     * @return void
     */
    public function textAndImage(string $text)
    {
        $response = Http::withToken($this->access_token)->post(
            $this->URI . '/ugcPosts',
            [
                "author" => "urn:li:person:".$this->person_id,
                "lifecycleState" => "PUBLISHED",
                "specificContent" => [
                    "com.linkedin.ugc.ShareContent" => [
                        "media" => [
                            [
                                "media" => $this->imageAsset,
                                "status" => "READY", // PROCESSING, READY,FAILED
                                "title" => [
                                    "attributes" => [],
                                    "text" => "img"
                                ]
                            ]
                        ],
                        "shareCommentary" => [
                            "attributes" => [],
                            "text" => $text
                        ],
                        "shareMediaCategory" => "IMAGE"
                    ]
                ],
                "targetAudience" => [
                    "targetedEntities" => [
                        [
                            "locations" => [
                                "urn:li:country:us",
                                "urn:li:country:gb"
                            ],
                            "seniorities" => [
                                "urn:li:seniority:3"
                            ]
                        ]
                    ]
                ],
                "visibility" => [
                    "com.linkedin.ugc.MemberNetworkVisibility" => "PUBLIC"
                ]
            ]
        )->json();
        return response()->json($response, 200);
    }

    /**
     * registerMediaUpload
     * 
     * @return void
     */
    public function registerMediaUpload()
    {
        $registerResponse = Http::withToken($this->access_token)->post(
            $this->URI . "/assets?action=registerUpload",
            [
                'registerUploadRequest' => [
                    'owner' => "urn:li:person:" . $this->person_id,
                    'recipes' => [
                        0 => 'urn:li:digitalmediaRecipe:feedshare-image',
                    ],
                    'serviceRelationships' => [
                        0 => [
                            'identifier' => 'urn:li:userGeneratedContent',
                            'relationshipType' => 'OWNER',
                        ],
                    ],
                    'supportedUploadMechanism' => [
                        0 => 'SYNCHRONOUS_UPLOAD',
                    ],
                ],
            ]
        )->throw()->json();
        //to be used for image upload
        $this->imageUploadURL = $registerResponse['value']['uploadMechanism']['com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest']['uploadUrl'];
        //the unique image asset id
        $this->imageAsset = $registerResponse['value']['asset'];
        return $registerResponse;
    }

    /**
     * uploadImage
     * to upload image on linkedin. However, the state of image will be unpublished
     * @param  mixed $image
     * @return object
     */
    public function uploadImage($image): object
    {
        try {
            $client = new \GuzzleHttp\Client();
            $result = $client->request('PUT', $this->imageUploadURL, [
                'headers' => ['Authorization' => 'Bearer ' . $this->access_token],
                'body' => fopen($image, 'r'),
            ]);
            return response()->json('Image uploaded [unpublised]', $result->getStatusCode());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * checkUploadStatus
     * Check if the status of the media upload
     * @return void
     */
    public function checkUploadStatus()
    {
        $assetID = substr(strrchr($this->imageAsset, ':'), 1);
        $response = Http::withToken($this->access_token)->get($this->URI . '/assets/' . $assetID)->throw()->json();
        return $response;
    }
}
