<?php

namespace App\Classes\Twitter;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Illuminate\Support\Facades\Http;

class TwitterAccount
{
    protected $app_api_key = 'mGZfJxILuMPCNZEeF6EAm8gXD';
    protected $app_api_secret_key = 'fs8Dk7LiMS2DzAUFeIVey06FOUewEqE83EInr6wHQzQtu2iqaC';
    protected $app_callback_url = 'https://auctionslive.com/';
    protected $base_uri = 'https://api.twitter.com';

    protected $oauth_token; 
    protected $oauth_token_secret;

    protected $client;

    /**
     * __construct
     *  setup oauth 1.0 Http client
     * @return void
     */
    public function __construct()
    {
        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key' => $this->app_api_key,
            'consumer_secret' => $this->app_api_secret_key,
            'signature_method' => Oauth1::SIGNATURE_METHOD_HMAC
        ]);
        $stack->push($middleware);

        $this->client = new Client([
            'base_uri' => $this->base_uri,
            'handler' => $stack,
            'auth' => 'oauth'
        ]);
    }

    public function requestToken()
    {
        $response = $this->client->post('/oauth/request_token', [
            'form_params' => [
                'oauth_callback' => $this->app_callback_url,
            ]
        ]);
        parse_str($response->getBody()->getContents(), $array_data);
        $this->oauth_token = $array_data['oauth_token'];
        $this->oauth_token_secret= $array_data['oauth_token_secret'];
        return $this->oauth_token;
        // now use the oauth_token in the GET url https://api.twitter.com/oauth/authorize?oauth_token={$this->oauth_token}
    }
}
