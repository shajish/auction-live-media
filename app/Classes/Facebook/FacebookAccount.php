<?php

namespace App\Classes\Facebook;

use Exception;
use Facebook\Facebook;
use Illuminate\Support\Facades\Http;

class FacebookAccount
{
    protected $URI = "https://graph.facebook.com/";
    private $app_id = '227777042284833';
    private $app_secret = 'a87e0a9d709aba50f43005171cb13eac';
    private $graph_version = 'v9.0';

    protected $fb;

    protected $long_lived_user_access_token;
    protected $long_lived_page_access_token; //facebook page
    
    // protected $facebook_business_account_id;

    public function __construct()
    {
        $this->fb = new Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => $this->graph_version
        ]);
    }

    /**
     * getUserAccount
     * Note:
     *  response data consists : user access token, page category, page permissions, page id, page name, 
     * @param  mixed $user_access_token
     * @param  mixed $user_id
     * @return  \Illuminate\Http\JsonResponse
     */

    public function getUserAccount($user_access_token, $user_id)
    {
        try {
            $response = $this->fb->get(
                '/' . $user_id . '/accounts',
                $user_access_token
            );
            $data = json_decode($response->getBody(), true);
            return response()->json($data, 200);
        } catch (Exception $e) {
            throw $e;
        }
    }

    
    /**
     * getLongLivedUserAccessToken
     * Note:
     *  Token valid for 60 days
     * 
     * @param  mixed $user_access_token
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLongLivedUserAccessToken($user_access_token)
    {
        try {
            $response = $this->fb->sendRequest(
                'GET',
                '/oauth/access_token',
                [
                    "grant_type" => "fb_exchange_token",
                    "client_id" => $this->app_id,
                    "client_secret" => $this->app_secret,
                    "fb_exchange_token" => $user_access_token
                ],
                $user_access_token
            );
            $data = json_decode($response->getBody(), true);
            return response()->json($data, 200);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * getLongLivedPageAccessToken
     * Note: 
     *  If you used a short-lived User access token, the Page access token is valid for 1 hour.
     *  If you used a long-lived User access token, the Page access token has no expiration date.
     * 
     * @param  mixed $long_lived_user_access_token
     * @param  mixed $page_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLongLivedPageAccessToken($long_lived_user_access_token, $page_id)
    {
        try {
            $response = $this->fb->sendRequest(
                'GET',
                '/' . $page_id,
                [
                    "fields" => "access_token",
                    "access_token" => $long_lived_user_access_token
                ],
            );
            $data = json_decode($response->getBody(), true);
            return response()->json($data, 200);
        } catch (Exception $e) {
            throw $e;
        }
    }
}
