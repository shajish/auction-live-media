<?php

namespace App\Classes\Facebook;

use App\Classes\Instagram\InstagramAccount;
use App\Classes\Instagram\InstagramPost;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FacebookPost extends FacebookAccount
{
    protected $page_id;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * to publish feeds to page
     */
    public function publishFeed(Request $request)
    {
        // dd($this->fb);
        $this->getPageDetails($request);
        $params = [];
        switch ($request->type) {
            case 'message':
                $params = [
                    'message' => $request->input('message'),
                ];
                break;
            case 'link':
                $params = [
                    'message' => $request->input('message'),
                    'link' => $request->input('link'),
                    'published' => '1', //1 to publish, 0 to create an unpublished post to be published later
                ];
                break;
            case 'image':
                //     $name = $request->file('image')->getClientOriginalName();
                //     $path = $request->file('image')->storeAs('public/images', $name);

                //     // TODO #1 NEED THIRD EYE!! upload image is not working
                //     $this->uploadImage($request->input('user_access_token'), $path);
                break;
        }

        try {
            $response = $this->fb->post(
                '/' . $this->page_id . '/feed',
                $params,
                $this->long_lived_page_access_token
            );
            return response()->json(json_decode($response->getBody()), 200);
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * getPageDetails
     * To get page_id and long lived page access token
     * @param  mixed $request
     * @return void
     */
    public function getPageDetails($request)
    {
        $user_access_token = $request->input('user_access_token');
        $user_id = $request->input('user_id');
        // get page_id
        $userProfile = $this->getUserAccount($user_access_token, $user_id);
        $this->page_id = $userProfile->getData()->data[0]->id; //facebook business page

        // obtain long lived user Access token
        $response = $this->getLongLivedUserAccessToken($user_access_token);
        $this->long_lived_user_access_token = $response->getData()->access_token;

        // get instagram the business accounts
        // $instaPost = new InstagramPost($this->long_lived_user_access_token,$this->page_id);
        // $instaPost->getAllPosts();
        
        // get long lived page access token
        $response = $this->getLongLivedPageAccessToken($this->long_lived_user_access_token, $this->page_id);
        $this->long_lived_page_access_token = $response->getData()->access_token;
    }

    // /**
    //  * TODO Still in progress
    //  * uploadImage
    //  * Connected to #1 TODO 
    //  * @param  mixed $token
    //  * @param  mixed $imagePath
    //  * @return void
    //  */
    // public function uploadImage($token, $imagePath)
    // {
    //     $data = [
    //         'message' => 'My awesome photo upload example.',
    //         'source' => $this->fb->fileToUpload('C:\Users\Auctions Live 21\Downloads\groot.jpg')
    //     ];
    //     $data = [
    //         'url' => 'https://cdn.wallpapersafari.com/13/26/Zq8vTY.jpg'
    //     ];
    //     try {
    //         $response = $this->fb->post('/' . $this->page_id . '/photos', $data, $token);
    //         return response()->json(json_decode($response->getBody()), 200);
    //     } catch (Exception $e) {
    //         throw $e;
    //     }
    // }


    // /**
    //  * Not working atm
    //  * publishVideo
    //  *
    //  * @param  mixed $request
    //  * @return void
    //  */
    // public function publishVideo(Request $request)
    // {

    //     $data = [
    //         'description' => 'My awesome video upload example.',
    //         'source' => $this->fb->fileToUpload('C:\Users\Auctions Live 21\Downloads\file_example_MOV_480_700kB.mov')
    //     ];

    //     try {
    //         // $response = $this->fb->post('/me/videos', $data, $request->input('user_access_token'));
    //         $response = $this->fb->post('/' . $this->page_id . '/videos', $data, $request->input('user_access_token'));
    //         return response()->json(json_decode($response->getBody()), 200);
    //     } catch (Exception $e) {
    //         throw $e;
    //     }
    // }

    // /**
    //  * @param Request $request
    //  * @return \Illuminate\Http\JsonResponse
    //  * publish a post with one image (using URL)
    //  */
    // public function publishImage(Request $request)
    // {
    //     $request->validate([
    //         //    'page_id' => 'required',
    //         //    'name' => 'required',
    //         //    'page_access_token' => 'required'
    //     ]);
    //     $response = Http::withToken($this->user_access_token)->post($this->URI . $this->page_id . "/photos", [
    //         'name' => 'level lawn and lock up garage with workshop. Set moments to the famed Bay Run on the shores of Iron Cove, Five Dock Park, cafes and shops.
    //                     21 Barnstaple Road, Five Dock NSW 2046
    //                     3🛏 2🛀 2🚗
    //                     📣📣📣AUCTION - SATURDAY 30 JANUARY 2021- 9:30AM
    //                     For for info contact JJK 0431678229
    //                     Agent - JJK
    //                     Powered by - Auction Live  https://auctionslive.com/
    //                     #Billproperty #AuctionLive #sydneyproperty #realestatesolution #sydneyrealestate',
    //         'access_token' => $this->page_access_token,
    //         'url' => 'https://static.dezeen.com/uploads/2020/02/house-in-the-landscape-niko-arcjitect-architecture-residential-russia-houses-khurtin_dezeen_2364_hero.jpg',

    //     ]);
    //     return response()->json($response->json(), 200);
    // }

}
