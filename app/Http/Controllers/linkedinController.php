<?php

namespace App\Http\Controllers;

use App\Classes\Linkedin\LinkedinAccount;
use Exception;
use Facebook\HttpClients\FacebookGuzzleHttpClient;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Classes\Linkedin\LinkedinPost;

class linkedinController extends Controller
{

    // private $URI = 'https://api.linkedin.com/v2';
    private $person_id; // linkedin_user_id
    private $access_token;
    private $linkedinAccount;
    private $linkedinPost;
    public function __construct()
    {
        $this->linkedinAccount = new LinkedinAccount();
        $this->linkedinPost = new LinkedinPost();
    }
    public function index()
    {
        return view('linkedinForm');
    }


    /**
     * store
     * Capable to perform 2 types of post : 
     * 1) with only text 
     * 2) with text and a single image
     * 
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'access_token' => 'required',
            'text' => 'required',
            'image' => 'nullable'
        ]);

        if ($validator->fails()) {
            return redirect('/linkedin')
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('image')) {
            $request->request->add(['type' => 'withImage']);
        } else {
            $request->request->add(['type' => 'onlyText']);
        }

        $response = $this->linkedinPost->publishContents($request);
        if ($response->isSuccessful()) {
            return redirect("/linkedin")->withSuccess('Success message');
        }
    }
}
