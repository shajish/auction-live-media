<?php

namespace App\Http\Controllers;

use App\Classes\Facebook\FacebookAccount;
use App\Classes\Facebook\FacebookPost;
use Exception;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FacebookController extends Controller
{
    private $URI = "https://graph.facebook.com/";
    private $facebookPost;

    public function __construct()
    {
        $this->facebookPost = new FacebookPost();
    }

    /**
     * index
     *
     * @param  mixed $request
     * @return void
     */
    public function index(Request $request)
    {
        return view('facebookForm');
    }

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'user_access_token' => 'required',
                'user_id' => 'required',
                'message' => 'required|string',
                'image' => 'image|mimes:jpg,png,jpeg,gif,svg|max:2048', //mimes:csv,txt,xlx,xls,pdf|max:2048|nullable',
                'link' => 'string|nullable'
            ]);

            if ($validator->fails()) {
                return redirect('/facebook')
                    ->withErrors($validator)
                    ->withInput();
            }

            // adding parameter in request to be more specific of post type

            // if ($request->hasFile('image')) {
            //     $request->request->add(['type' => 'image']);
            // } else
            if ($request->filled('link')) {
                $request->request->add(['type' => 'link']);
            } else {
                $request->request->add(['type' => 'message']);
            }

            $response = $this->facebookPost->publishFeed($request);

            if ($response->isSuccessful()) {
                return Redirect::to("/facebook")->withSuccess('Success message');
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
