## Social media posting
This project aims to automate the content posting in social media platform. The success of this project will definitely ease digital marketing process will less hastle.

Covered social media platforms
1. Facebook
2. Instagram
3. Linkedin 

### FACEBOOK 

In order to enjoy facebook posting features via this project, the client should have a facebook app setup which is verified to get certain user [permissions](https://developers.facebook.com/docs/permissions/reference/).  [This link](https://developers.facebook.com/apps/227777042284833/app-review/submissions/current-request/?business_id=456486732190077) has all the essentials regarding getting permissions.

Meanwhile, test-users with any essential permissions can be created from facebook developer dashboard, to experiment with the project. Here is the credentials of a test user created during the development process.

> username : open_hwbrozw_user@tfbnw.net

> password : test@123


#### User Login
The prototype uses The [Facebook JavaScript SDK](https://developers.facebook.com/docs/facebook-login/web) for signing in our users.
It is recommended to consider the freedom for users to allow, revoke or deny the permission request. Do check the [guideline](https://developers.facebook.com/docs/facebook-login/permissions/requesting-and-revoking/#requesting---revoking-permissions) provided in facebook developer resource. 

#### Current Stat
This prototype is able to post textual feeds on facebook at the moment. Certain changes in the facebook policy has created problems while posting media files. Please check the following references to be familiar with the updates.

However, reference no. 3 says that, previously image uploads are preformed by first uploading the image which generates image id and then using thet image id to post feed on facebook.

**References**
1. [facebook policy update](https://developers.facebook.com/blog/post/2018/04/24/new-facebook-platform-product-changes-policy-updates/)
2. [share dialog](https://developers.facebook.com/docs/sharing/reference/share-dialog)  
3. [photo upload discussion](https://stackoverflow.com/questions/65089225/facebook-graph-api-page-post-with-multiple-photos)
4. [photo upload facebook doc](https://developers.facebook.com/docs/graph-api/reference/page/photos/#multi)
5. [page post](https://developers.facebook.com/docs/graph-api/reference/page-post/#overview)
6. [facebook php SDK](https://github.com/facebookarchive/php-graph-sdk/tree/master/docs)
7. [facebook php SDK : file upload](https://developers.facebook.com/docs/php/FacebookFile/5.0.0)
8. [video upload facebook doc](https://developers.facebook.com/docs/video-api/guides/publishing)
9. [video upload discussion: with a solution suggested](https://stackoverflow.com/questions/38237375/how-to-upload-video-to-facebook-page-using-php-sdk-and-api-graph-2-6)
10. [Getting long lived access tokens](https://developers.facebook.com/docs/pages/access-tokens/#get-a-page-access-token)



## LINKEDIN

### User login
The linkedin uses 3 step authenciation process as mentioned in their [official documentation](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-auth-code-flow#redirect-uri-setup-required-for-single-page-apps). that are:  
1. Get authentication tokens
2. Get access token using the authentication token
3. Use the access token to perform other actions

**NOTE:** 
- [_lifespan of access token last till 60days_](https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#step-3-exchange-authorization-code-for-an-access-token)
- To refresh token we basically have to repeat the authorization process again. [{reference}](https://docs.microsoft.com/en-us/linkedin/shared/authentication/authorization-code-flow#step-5-refresh-access-token) 
- Currently, only jpeg and png formats are supported. The media file size should be under 10 MB for all file types. All image files have a pixel restriction of 40,000,000, and you may only upload one media object per API call. [{reference}](https://docs.microsoft.com/en-us/linkedin/marketing/integrations/community-management/shares/rich-media-shares#:~:text=The%20media%20file%20size%20should,media%20object%20per%20API%20call.)

You can use the obtained access token in this prototype to perform further actions. 

### organization access
In order to get permission for organization pages, The app must have _MARKETING DEVELOPER PLATFORM_ product. 



