<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\linkedinController;
use App\Http\Controllers\TwitterContoller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return asset('storage/images/house2.jpg');
//    return $request->user();
});

Route::get('/user/me/{user_access_token}',[FacebookController::class,'getUserProfile']);
Route::get('/user/accounts',[FacebookController::class,'getUserAccount']);
Route::post('/user/page/images',[FacebookController::class,'publishImage']);
Route::post('/user/page/feed',[FacebookController::class,'publishFeed']);
Route::get('/user/page/videos',[FacebookController::class,'publishVideo']);

Route::get('linkedin/login',[linkedinController::class,'login']);
//can be used when we use our own redirect url in linkedin app.
Route::get('linkedin/user',[linkedinController::class,'getStoreAuthorizationCode']);

Route::post('/linkedin/user/access-token',[linkedinController::class,'getAccessCode']);


Route::get('twitter',[TwitterContoller::class,'index']);
Route::get('twitter/test',[TwitterContoller::class,'getToken']);
